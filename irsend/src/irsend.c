/*
===============================================================================
Dieses Programm ist freie Software. Sie koennen es unter den Bedingungen de
GNU General Public License, wie von der Free Software Foundation veroeffentlicht
weitergeben und/oder modifizieren, entweder gemaess Version 3 der Lizenz oder
(nach Ihrer Option) jeder spaeteren Version.

irsend.c

irsend wird wie folgt aufgerufen.

irsend <Geraetename> <Kommandoname>

Die parameter werden an die parse() uebergeben, von parse() gefuellte struct
wird an den Treiber weitergereicht.

written by TaK-Team
===============================================================================
*/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <fcntl.h>		/* open */
#include <unistd.h>		/* exit */
#include <sys/ioctl.h>		/* ioctl */
#include <sched.h>
#include "parser.h"

#define DEVICE_FILE_NAME "/dev/irmod"


int main( int argc, char *argv[] )
{
	int file_desc, ret_val;

	struct ircstruct ircode;

/* Ueberpruefung ob die Anzahl der Argumente <3 ist. */
	
	if ( argc < 3 ) {

		printf("Benötige mindesten 2 Argumente!\n");
		printf("Aufruf: %s <Config> <KEY_BUTTON>...\n", *argv);

	return EXIT_FAILURE;
   	}

	/* Aufruf des Parsers. */
	
	ret_val = parse( &ircode, argv );

	if (ret_val < 0) {
		
		return EXIT_FAILURE;
	}

/* Oeffnen des device files /dev/irmode. */

	file_desc = open(DEVICE_FILE_NAME, 0);

	if (file_desc < 0) {
		printf("Can't open device file: %s\n", DEVICE_FILE_NAME);
		exit(-1);
	}

/* Sendefunktion des Treibers wird mit ioctl angestossen. */

	ret_val = ioctl(file_desc, 42, &ircode );

	if (ret_val < 0) {

		printf("ioctl_set_msg failed:%d\n", ret_val);
		exit(-1);

	}

		return 0;
}
