/*
===============================================================================
Dieses Programm ist freie Software. Sie koennen es unter den Bedingungen de
GNU General Public License, wie von der Free Software Foundation veroeffentlicht
weitergeben und/oder modifizieren, entweder gemaess Version 3 der Lizenz oder
(nach Ihrer Option) jeder spaeteren Version.

parser.h

globale Definition des IR-structs und des Funktionprototypes parse.

written by TaK-Tesm
===============================================================================
*/

#ifndef PARSER_H
#define PARSER_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct ircstruct{
    unsigned int sb_pulse;
    unsigned int sb_space;
    unsigned int adress;
    unsigned int command;
    unsigned int stop;
    };

int parse( struct ircstruct *ircode, char *argv[] );

#endif /* PARSER_H */
