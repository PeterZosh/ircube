#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/delay.h>
#include <linux/fs.h>
#include <linux/init.h>
#include <linux/cdev.h>
#include <linux/device.h>

#include <asm/io.h>
#include <asm/uaccess.h>

#include <mach/hardware.h>
#include <mach/gpio.h>

#define PULSE 24
#define DEVICE_NAME "irmod"


MODULE_LICENSE( "GPL" );
MODULE_AUTHOR( "Tobias Theile <tobias.theile@theile.me>" );
MODULE_AUTHOR( "Alexander Koch <alexander.koch@atomtown.de>" );
MODULE_DESCRIPTION( "irCube infrared driver" );
MODULE_SUPPORTED_DEVICE( "irCube based on GNUblin" );


static struct cdev irmod_dev;
static struct class *dev_class;
static struct device *device;
static dev_t dev_num;

/* Deklaration vom irstruct. */

struct ircstruct{
        unsigned int sb_pulse;
        unsigned int sb_space;
        unsigned int adress;
        unsigned int command;
        unsigned int stop;
};

/* Funktionsprototypen des Treibers. */

static void ircode_out( struct ircstruct *ircode );
static void pulse_send( unsigned long lenght );
static void space_send( unsigned long slenght );
static int device_ioctl( struct inode *inode, struct file *file,
                         unsigned int cmd, unsigned long ioctl_param);

/*
===============================================================================
Jedes Highbit wird auf die Traegerfrequenz aufmoduliert.
===============================================================================
*/

static void pulse_send( unsigned long lenght )
{
	int flag;
	unsigned long t, act;

	t = PULSE / 2;
	flag = 1;
	act = 0;

/* 	ca 1us zusaetzliche Zeit bedingt durch while schleife
	daher wird mit lenght/t die haeufigkeit der zu durchlaufenden
	while-schleife ermittelt und von der eigentlichen pulse zeit
	abgezogen 
*/

    while ( act <= ( lenght - (lenght / t) ) ) {

		gpio_set_value( GPIO_GPIO11, flag );

		udelay( t );

		act += t;

		flag = !flag;

	}


}

/*
===============================================================================
GPIO Ausgang auf Low legen fuer in slenght bestimmte Zeit.
===============================================================================
*/

static void space_send( unsigned long slenght )
{

	gpio_set_value( GPIO_GPIO11, 0 );

	if ( slenght < 2000 ) {	

		udelay( slenght );

	} else {

		mdelay( (slenght / 1000) );

	}

}

/*
===============================================================================
Modulierung des NEC Infrarotkodes anhang des von irsend gereichten structs.
=============================================================================== 
*/

static void ircode_out( struct ircstruct *ircode )
{
	int i = 0;
	int binary = 0;
	unsigned long cpuflags;
	unsigned int hexcode;
	spinlock_t lock = SPIN_LOCK_UNLOCKED;


	hexcode = ircode->command + ( ircode->adress << 16 );

	spin_lock_init( &lock );

	/* Setzen von Spinlocks um die Sendeprozedur durch die IRQ's nicht
	zu stoeren. */

	spin_lock_irqsave( &lock , cpuflags );

	pulse_send( ircode->sb_pulse );
	space_send( ircode->sb_space );

	/* Bitweise lesen des Adress/Commandoblocks (32 bit) */

  	for ( i = 31 ; i >= 0 ; i-- ) {

		binary = ( hexcode >> i ) & 1;

		if ( binary ) {

		/* binaere 1 modelieren */

			pulse_send( 560 );
			space_send( 1690 );

		} else {

		/* binaere 0 modelieren */

			pulse_send( 560 );
			space_send( 560 );
		}

	}

	/* Stopbitmodulation */

	pulse_send( ircode->stop );

	spin_unlock_irqrestore( &lock , cpuflags );

}

/*
===============================================================================
Anhand der ioctl Nummer wird die Sendefunktionalitaet des Treibers angestossen.
Ein Kommentar zu dieser Zeile schreiben
===============================================================================
*/

static struct file_operations fops = {
    .owner      =   THIS_MODULE,
    .write      =   NULL,
    .ioctl      =   device_ioctl,
    .read       =   NULL,
    .poll       =   NULL,
    .open       =   NULL,
    .release    =   NULL,
    .llseek     =   NULL,
};

static int device_ioctl( struct inode *inode, struct file *file,
unsigned int cmd, unsigned long ioctl_param )
{

    struct ircstruct *ircode = kmalloc( sizeof(struct ircstruct), GFP_KERNEL );

	switch( cmd ) {

		case 42:
			if (copy_from_user( ircode, (void *)ioctl_param, 
								sizeof( struct ircstruct)) != 0 ) {

				printk( KERN_INFO "Copy from userpace failed.\n" );
				return -EFAULT;
			}
			ircode_out( ircode );
			kfree( ircode );
			break;

		default:
			printk( KERN_INFO " unknown ioctl commando.\n" );
			break;
}

	return 0;
}

/*
===============================================================================
Initialisierund des Treibers.
===============================================================================
*/

int init_module(void)
{

	printk ( KERN_INFO "irmod for irCube loading.\n" );

	if ( alloc_chrdev_region( &dev_num , 0, 1, DEVICE_NAME ) < 0 ) {

        printk( KERN_ALERT "Major number allocation failed\n" );

		return -1;
	
    }

	dev_class = class_create( THIS_MODULE, DEVICE_NAME );

	if ( IS_ERR( dev_class ) ) {

		printk( KERN_ALERT "creating device class failed.\n" );
	
		unregister_chrdev_region( dev_num, 1 );

		return -1;
	}

	cdev_init( &irmod_dev, &fops );

	cdev_add( &irmod_dev, dev_num , 1 );

	device = device_create( dev_class, NULL, MKDEV( MAJOR (dev_num), 0),
				 NULL, DEVICE_NAME );

	if ( IS_ERR( device ) ) {

		printk( KERN_ALERT "creating device failed.\n" );
	
		class_destroy( dev_class );
	
		return -1;
	}

	return 0;

}

/*
===============================================================================
Exitroutine des Treibers.
===============================================================================
*/

void cleanup_module(void)
{

	cdev_del( &irmod_dev );
	
	unregister_chrdev_region( dev_num, 1 );

	device_destroy( dev_class, dev_num );

	class_destroy( dev_class );

	printk ( KERN_INFO "irmod for irCube unloaded.\n" );

}
