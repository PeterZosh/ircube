<!-- Dieses Programm ist freie Software. Sie koennen es unter den Bedingungen de
GNU General Public License, wie von der Free Software Foundation veroeffentlicht
weitergeben und/oder modifizieren, entweder gemaess Version 3 der Lizenz oder
(nach Ihrer Option) jeder spaeteren Version. -->

<!-- written by TaK-Team -->

<html>
	<head>
		<title>irCube by TaK</title>
		<meta name="viewport" content="width=device-width">
		<meta name="viewport" content="initial-scale=1.0, user-scalable=no">
		<meta name="apple-mobile-web-app-capable" content="yes">
	</head>

	<body background="img/wallpaper/wallpaper.png">
		<table border=0>
			<tr align="center" valign="middle">
				<td style="height:100px; width:300px">
					<a href="sat.php"><img src="img/remotes/sat.png" border="0" alt=""></a>
				</td>
			</tr>
			<tr align="center" valign="middle">
				<td style="height:100px; width:300px">
					<a href="teufel.php"><img src="img/remotes/teufel.png" border="0" alt=""></a>
				</td>
			</tr>
		</table>
	</body>
</html>
