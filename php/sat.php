<!-- Dieses Programm ist freie Software. Sie koennen es unter den Bedingungen de
GNU General Public License, wie von der Free Software Foundation veroeffentlicht
weitergeben und/oder modifizieren, entweder gemaess Version 3 der Lizenz oder
(nach Ihrer Option) jeder spaeteren Version. -->

<!-- written by TaK-Team -->

<html>
	<head>
		<title>irCube by TaK</title>
		<meta name="viewport" content="width=device-width">
		<meta name="viewport" content="initial-scale=1.0, user-scalable=no">
		<meta name="apple-mobile-web-app-capable" content="yes">
	</head>

	<body background="img/wallpaper/wallpaper.png">
		<form action="" method="post">

            <table border=0>
				<tr align="center" valign="middle">
                    <td style="height:100px; width:100px"><input type="image" src="img/buttons/mute.png" name="mute"></td>
					<td style="height:100px; width:100px"><input type="image" src="img/buttons/up.png" name="zero"></td>
					<td style="height:100px; width:100px"><input type="image" src="img/buttons/power.png" name="power"></td>
				</tr>
				<tr align="center" valign="middle">
					<td style="height:100px; width:100px"><input type="image" src="img/buttons/left.png" name="exit"></td>
					<td style="height:100px; width:100px"><input type="image" src="img/buttons/ok.png" name="zero"></td>
					<td style="height:100px; width:100px"><input type="image" src="img/buttons/right.png" name="back"></td>
				</tr>
				<tr align="center" valign="middle">
                    <td style="height:100px; width:100px"><input type="image" src="img/buttons/vdown.png" name="vdown"></td>
					<td style="height:100px; width:100px"><input type="image" src="img/buttons/down.png" name="zero"></td>
                    <td style="height:100px; width:100px"><input type="image" src="img/buttons/vup.png" name="vup"></td>
				</tr>
                <tr align="center" valign="middle">
                    <td style="height:100px; width:100px"><input type="image" src="img/buttons/pdown.png" name="pdown"></td>
                    <td style="height:100px; width:100px"><input type="image" src="img/buttons/info.png" name="info"></td>
                    <td style="height:100px; width:100px"><input type="image" src="img/buttons/pup.png" name="pup"></td>
                </tr>
				<tr align="center" valign="middle">
					<td style="height:100px; width:100px"><input type="image" src="img/buttons/exit.png" name="exit"></td>
					<td style="height:100px; width:100px">
						<a href="numblock.php"><img src="img/buttons/num.png" border="0" alt=""></a>
					<td style="height:100px; width:100px"><input type="image" src="img/buttons/back.png" name="back"></td>
				</tr>
			</table>

			<br>
			</br>

			<table border=0>
				<tr align="center" valign="middle">
					<td style="height:100px; width:300px">
						<a href="index.php"><img src="img/remotes/home.png" border="0" alt=""></a>
					</td>
				</tr>
			</table>
		</form>
	
		<?php

			$remote = "sat";

			if(isset($_POST['power_x'] )) {
				$output = "<pre>".shell_exec("irsend $remote POWER")."</pre>";
				echo $output;
			}

			if(isset($_POST['one_x'] )) {
				$output = "<pre>".shell_exec("irsend $remote ONE")."</pre>";
				echo $output;
			}

			if(isset($_POST['two_x'] )) {
				$output = "<pre>".shell_exec("irsend $remote TWO")."</pre>";
				echo $output;
			}

			if(isset($_POST['three_x'] )) {
				$output = "<pre>".shell_exec("irsend $remote THREE")."</pre>";
				echo $output;
			}

			if(isset($_POST['four_x'] )) {
				$output = "<pre>".shell_exec("irsend $remote FOUR")."</pre>";
				echo $output;
			}

			if(isset($_POST['five_x'] )) {
				$output = "<pre>".shell_exec("irsend $remote FIVE")."</pre>";
				echo $output;
			}

			if(isset($_POST['six_x'] )) {
				$output = "<pre>".shell_exec("irsend $remote SIX")."</pre>";
				echo $output;
			}

			if(isset($_POST['seven_x'] )) {
				$output = "<pre>".shell_exec("irsend $remote SEVEN")."</pre>";
				echo $output;
			}
				
			if(isset($_POST['eight_x'] )) {
				$output = "<pre>".shell_exec("irsend $remote EIGHT")."</pre>";
				echo $output;
			}

			if(isset($_POST['nine_x'] )) {
				$output = "<pre>".shell_exec("irsend $remote NINE")."</pre>";
				echo $output;
			}

			if(isset($_POST['zero_x'] )) {
				$output = "<pre>".shell_exec("irsend $remote ZERO")."</pre>";
				echo $output;
			}

			if(isset($_POST['back_x'] )) {
				$output = "<pre>".shell_exec("irsend $remote BACK")."</pre>";
				echo $output;
			}

			if(isset($_POST['exit_x'] )) {
				$output = "<pre>".shell_exec("irsend $remote EXIT")."</pre>";
				echo $output;
			}

			if(isset($_POST['vdown_x'] )) {
				$output = "<pre>".shell_exec("irsend $remote VDOWN")."</pre>";
				echo $output;
			}

			if(isset($_POST['mute_x'] )) {
				$output = "<pre>".shell_exec("irsend $remote MUTE")."</pre>";
				echo $output;
			}

			if(isset($_POST['vup_x'] )) {
				$output = "<pre>".shell_exec("irsend $remote VUP")."</pre>";
				echo $output;
			}

			if(isset($_POST['pdown_x'] )) {
				$output = "<pre>".shell_exec("irsend $remote PDOWN")."</pre>";
				echo $output;
			}

			if(isset($_POST['info_x'] )) {
				$output = "<pre>".shell_exec("irsend $remote INFO")."</pre>";
				echo $output;
			}

			if(isset($_POST['pup_x'] )) {
				$output = "<pre>".shell_exec("irsend $remote PUP")."</pre>";
				echo $output;
			}
		?>
	</body>
</html>
