<!-- Dieses Programm ist freie Software. Sie koennen es unter den Bedingungen de
GNU General Public License, wie von der Free Software Foundation veroeffentlicht
weitergeben und/oder modifizieren, entweder gemaess Version 3 der Lizenz oder
(nach Ihrer Option) jeder spaeteren Version. -->

<!-- written by TaK-Team -->

<html>
	<head>
		<title>irCube by TaK</title>
		<meta name="viewport" content="width=device-width">
		<meta name="viewport" content="initial-scale=1.0, user-scalable=no">
		<meta name="apple-mobile-web-app-capable" content="yes">
	</head>

	<body background="img/wallpaper/wallpaper.png">
		<form action="" method="post">

			<table border=0>
				<tr align="center" valign="middle">
					<td style="height:100px; width:100px"><input type="image" src="img/buttons/one.png" name="one"></td>
					<td style="height:100px; width:100px"><input type="image" src="img/buttons/two.png" name="two"></td>
					<td style="height:100px; width:100px"><input type="image" src="img/buttons/three.png" name="three"></td>
				</tr>
				<tr align="center" valign="middle">
					<td style="height:100px; width:100px"><input type="image" src="img/buttons/four.png" name="four"></td>
					<td style="height:100px; width:100px"><input type="image" src="img/buttons/five.png" name="five"></td>
					<td style="height:100px; width:100px"><input type="image" src="img/buttons/six.png" name="six"></td>
				</tr>
				<tr align="center" valign="middle">
					<td style="height:100px; width:100px"><input type="image" src="img/buttons/seven.png" name="seven"></td>
					<td style="height:100px; width:100px"><input type="image" src="img/buttons/eight.png" name="eight"></td>
					<td style="height:100px; width:100px"><input type="image" src="img/buttons/nine.png" name="nine"></td>
				</tr>
				<tr align="center" valign="middle">
					<td style="height:100px; width:100px">
						<a href="sat.php"><img src="img/buttons/left.png" border="0" alt=""></a>
					</td>
					<td style="height:100px; width:100px"><input type="image" src="img/buttons/zero.png" name="zero"></td>
					<td style="height:100px; width:100px"><input type="image" src="img/buttons/clear.png" name="back"></td>
				</tr>
			</table>
		</form>
	
		<?php

			$remote = "sat";

			if(isset($_POST['one_x'] )) {
				$output = "<pre>".shell_exec("irsend $remote ONE")."</pre>";
				echo $output;
			}

			if(isset($_POST['two_x'] )) {
				$output = "<pre>".shell_exec("irsend $remote TWO")."</pre>";
				echo $output;
			}

			if(isset($_POST['three_x'] )) {
				$output = "<pre>".shell_exec("irsend $remote THREE")."</pre>";
				echo $output;
			}

			if(isset($_POST['four_x'] )) {
				$output = "<pre>".shell_exec("irsend $remote FOUR")."</pre>";
				echo $output;
			}

			if(isset($_POST['five_x'] )) {
				$output = "<pre>".shell_exec("irsend $remote FIVE")."</pre>";
				echo $output;
			}

			if(isset($_POST['six_x'] )) {
				$output = "<pre>".shell_exec("irsend $remote SIX")."</pre>";
				echo $output;
			}

			if(isset($_POST['seven_x'] )) {
				$output = "<pre>".shell_exec("irsend $remote SEVEN")."</pre>";
				echo $output;
			}
				
			if(isset($_POST['eight_x'] )) {
				$output = "<pre>".shell_exec("irsend $remote EIGHT")."</pre>";
				echo $output;
			}

			if(isset($_POST['nine_x'] )) {
				$output = "<pre>".shell_exec("irsend $remote NINE")."</pre>";
				echo $output;
			}

			if(isset($_POST['zero_x'] )) {
				$output = "<pre>".shell_exec("irsend $remote ZERO")."</pre>";
				echo $output;
			}

		?>
	</body>
</html>
